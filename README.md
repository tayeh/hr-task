This is an awesome web application built with Node.js and deployed to a Kubernetes cluster using Terraform and GitLab CI/CD.

## Running the app locally

To run the app locally, you'll need to have Docker installed on your machine. Once you have Docker installed, follow these steps:

- Clone the repository to your local machine: `git clone https://github.com/yourusername/my-awesome-app.git`
- Navigate to the project directory: `cd my-awesome-app`
- Build the Docker image: `docker build -t my-awesome-app .`
- Run the Docker container: `docker run -p 3000:3000 my-awesome-app`
- Open your web browser and go to http://localhost:3000

## Provisioning cloud resources with Terraform

To provision cloud resources with Terraform, you'll need to have Terraform installed on your machine and have the necessary credentials for your cloud provider. Once you have Terraform installed and your credentials set up, follow these steps:

- Clone the repository to your local machine: `git clone https://github.com/yourusername/my-awesome-app.git`
- Navigate to the terraform directory: `cd my-awesome-app/terraform`
- Initialize Terraform: `terraform init`
- Plan the infrastructure changes: `terraform plan`
- Apply the infrastructure changes: `terraform apply`


The GitLab CI/CD pipeline is triggered automatically whenever there's a change in the source code repository. The pipeline will build the Docker image, push it to your Docker registry, and deploy the updated image to your Kubernetes cluster.
