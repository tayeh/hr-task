const express = require('express');
const app = express();
const port = 3000;

app.get('/', (req, res) => {
  let currentTimestamp = new Date();
  res.send(`Hello, the current timestamp is ${currentTimestamp}`);
});

app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});
