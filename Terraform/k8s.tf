resource "kubernetes_deployment" "my_app" {
  metadata {
    name = "my-app-deployment"
  }

  spec {
    replicas = 3

    selector {
      match_labels = {
        app = "my-app"
      }
    }

    template {
      metadata {
        labels = {
          app = "my-app"
        }
      }

      spec {
        container {
          image = "tayeh/my-express-app:latest"
          name  = "my-app"
        }
      }
    }
  }
}

resource "kubernetes_service" "my_app" {
  metadata {
    name = "my-app-service"
  }

  spec {
    selector = {
      app = "my-app"
    }

    port {
      port        = 80
      target_port = 3000
    }

    type = "LoadBalancer"
  }
}

output "service_status" {
  description = "Status of the Service"
  value       = kubernetes_service.my_app.status
}
