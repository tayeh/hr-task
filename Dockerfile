# Using the Fedora Minimal base image
FROM registry.fedoraproject.org/fedora-minimal:latest

# Creating app directory
WORKDIR /usr/src/app

# Installing Node.js
RUN dnf5 -y upgrade && \
    dnf5 -y install nodejs npm && \
    dnf5 clean all

# Copy package.json and package-lock.json
COPY package*.json ./

# Install app dependencies
RUN npm install

# Bundle app source
COPY . .

# Exposing port
EXPOSE 3000

# Running the app
CMD [ "node", "app.js" ]
